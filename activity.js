// #1
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    { 
        $count: "fruitsOnSale"
    }
]);

// #2
db.fruits.aggregate([
    {
        $match: {
            stock: {
                $gte: 20
            }
        }
    },
    { 
        $count: "enoughStock"
    }
]);

// #3
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier_id", 
            average_price: {
                $avg: "$price"
            }
        }
    }
]);

// #4
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier_id", 
            highest_price: {
                $max: "$price"
            }
        }
    }
]);

// #5
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier_id", 
            lowest_price: {
                $min: "$price"
            }
        }
    }
]);
